package com.mitocode.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
//import javax.enterprise.context.SessionScoped;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.mindrot.jbcrypt.BCrypt;
import org.primefaces.PrimeFaces;

import com.mitocode.model.Usuario;
import com.mitocode.service.IUsuarioService;

@Named
@ViewScoped
public class UsuarioBean implements Serializable{
	
	@Inject
	private IUsuarioService service;
	private Usuario usuarios;

	private String Busca="";
	private String contraseña="";
	private boolean enabled = false;
	
	@PostConstruct
	public void init() {
		//this.usuarios=new Usuario();
		//this.listarUsuarios();
	}
		
	public void listarUsuarios() {
		try {
			this.usuarios = this.service.leerPorNombreUsuario(Busca);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void verificaContraseña() {
		System.out.println("Contraseña Ingresada: "+ contraseña);
		System.out.println("presiona boton 2");
		String claveHash = this.usuarios.getContrasena();
		boolean verifica = this.service.verificaC(contraseña, claveHash);
		System.out.println("Contraseña Ingresada: "+ contraseña +" claveHash: "+ claveHash);
		if (verifica == true) {
			System.out.println("Iguales");
			this.enabled = true;
		}else {
			System.out.println("No iguales");
			this.enabled = false;
		}
	}
	
	public void btncancelar() {
		System.out.println("entro en btn cancelar");
		this.enabled = false;
	}
	
	public void btnmodificar() {
		System.out.println("entro en btn modificar");
		this.contraseña="";
	}
	
	public void modificar() throws Exception {
		String clave = this.usuarios.getContrasena();
		String claveHash = BCrypt.hashpw(clave, BCrypt.gensalt());
		this.usuarios.setContrasena(claveHash);
		this.service.modificar(usuarios);
		System.out.println("entro en proceso de modificar");
	}


	public Usuario getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(Usuario usuarios) {
		this.usuarios = usuarios;
	}
	
	
	public String getBusca() {
		return Busca;
	}

	public void setBusca(String busca) {
		Busca = busca;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getContraseña() {
		return contraseña;
	}

	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}



	

	
	
}
