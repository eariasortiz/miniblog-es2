package com.mitocode.service;

import java.util.List;

import com.mitocode.model.Persona;
import com.mitocode.model.PublicadorSeguidor;
import com.mitocode.model.Usuario;

public interface IUsuarioService {

	Usuario leerPorNombreUsuario(String us);
	Usuario login(Usuario us);
	boolean verificaC(String clave, String claveHash);
	Integer modificar(Usuario t) throws Exception;
}
