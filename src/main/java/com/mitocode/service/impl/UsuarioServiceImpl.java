package com.mitocode.service.impl;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.inject.Named;

import org.mindrot.jbcrypt.BCrypt;

import com.mitocode.dao.IUsuarioDAO;
import com.mitocode.model.Usuario;
import com.mitocode.service.IUsuarioService;

@Named
public class UsuarioServiceImpl implements Serializable, IUsuarioService  {

	@EJB
	private IUsuarioDAO dao;

	@Override
	public Usuario login(Usuario us) {
		String clave = us.getContrasena();
		String claveHash = dao.traerPassHashed(us.getUsuario());

		try {
			if (BCrypt.checkpw(clave, claveHash)) {
				return dao.leerPorNombreUsuario(us.getUsuario());
			}
		} catch (Exception e) {
			throw e;
		}

		return new Usuario();
	}
	
	
	
	@Override
	public boolean verificaC(String clave, String claveHash) {
		if (BCrypt.checkpw(clave, claveHash)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	@Override
	public Usuario leerPorNombreUsuario(String us) {
		return dao.leerPorNombreUsuario(us);
	}

	@Override
	public Integer modificar(Usuario t) throws Exception {
		return dao.modificar(t);
	}
}
